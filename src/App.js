import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ProductDetails from "./pages/ProductDetails";
import ProductList from "./pages/ProductList";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<ProductList />} />
        <Route path="/details" element={<ProductDetails />} />
      </Routes>
    </Router>
  );
};

export default App;