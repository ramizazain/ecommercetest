import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./ProductList.css";

const ProductList = () => {
  const navigate = useNavigate();

  const [products, setProducts] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const [searchKey, setSearchKey] = useState("");

  const searchProducts = (event) => {
    setSearchKey(event.target.value);
    setProducts(
      allProducts.filter((product) => product.title.includes(searchKey))
    );
  };

  const onClickProduct = (selectedProduct) => {
    navigate("/details", { state: selectedProduct });
  };

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((result) => {
        setProducts(result);
        setAllProducts(result);
      });
  }, []);
  return (
    <div className="container">
      <div className="header">
        <input
          className="searchBar"
          value={searchKey}
          placeholder={"Search products here.."}
          onChange={(e) => searchProducts(e)}
        />
      </div>
      <ul className="productList">
        {products.map((product, index) => (
          <li
            className="product"
            key={index}
            onClick={()=>onClickProduct(product)}
          >
            <img
              src={product.image}
              alt={"img"}
              className={"productImage"}
            />
            <span className="productName">{product.title}</span>
            <span className="productPrice">$ {product.price}</span>
            <button className="button" onClick={() => {}}>
              Add to bag
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProductList;