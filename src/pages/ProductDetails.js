import React from "react";
import "./ProductDetails.css";
import { useLocation } from "react-router-dom";

const ProductDetails = () => {
  const location = useLocation();
  const selectedProduct = location.state;

  return (
    <div className="detailsPageContainer">
      <img src={selectedProduct.image} alt={"img"} className="detailImage" />
      <div className="detailsContainer">
        <span className="detailsName">{selectedProduct.title}</span>
        <span className="detailsDescription">
          {selectedProduct.description}
        </span>
        <span className="detailsPrice">Price : ${selectedProduct.price}</span>
        <span className="detailsCategory">
          Category : ${selectedProduct.category}
        </span>
        <button className="detailButton" onClick={() => {}}>
          Add to bag
        </button>
      </div>
    </div>
  );
};

export default ProductDetails